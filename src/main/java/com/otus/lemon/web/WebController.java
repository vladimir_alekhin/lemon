package com.otus.lemon.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {
    @GetMapping("/lemon")
    public String lemon() {
        return "index.html";
    }
}
