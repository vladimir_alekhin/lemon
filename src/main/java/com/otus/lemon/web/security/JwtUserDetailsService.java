package com.otus.lemon.web.security;

import com.otus.lemon.data.UserRepository;
import com.otus.lemon.data.model.CustomUserDetails;
import com.otus.lemon.data.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = userRepository.findByUsername(username);
            return new CustomUserDetails(user.getUsername(), user.getPassword(), Collections.emptyList(), user);
        } catch (Exception e) {
            log.error("User not found for username: " + username, e);
            throw new UsernameNotFoundException("User not found for username: " + username, e);
        }
    }
}
