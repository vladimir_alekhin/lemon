package com.otus.lemon.web.security;

import lombok.Data;

@Data
public class JwtRequest {
    private String username;
    private String password;
}
