package com.otus.lemon.web.security;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -1121241832030827797L;
    private final String token;
}
