package com.otus.lemon.data.test.generator;


import com.github.javafaker.Faker;
import com.otus.lemon.data.UserRepository;
import com.otus.lemon.data.model.Friendship;
import com.otus.lemon.data.model.Gender;
import com.otus.lemon.data.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Slf4j
@Component
@ConditionalOnProperty(name = "test.data.generator.enabled", havingValue = "true")
public class TestDataGenerator {

    private final Long profileCount;
    private final int chunkSize = 1000;
    private final String mode;
    private final UserRepository userRepository;
    private final Faker faker;
    private final Random random;
    private final String password;
    private final ExecutorService executorService = Executors.newFixedThreadPool(12);

    private final List<String> firstNames = new ArrayList<>(1000);
    private final List<String> lastNames = new ArrayList<>(1000);
    private final List<String> cities = new ArrayList<>(1000);
    private final List<String> jobs = new ArrayList<>(1000);
    private final List<String> beers = new ArrayList<>(1000);


    public TestDataGenerator(@Value("${test.data.generator.profile.count}") Long profileCount,
                             @Value("${test.data.generator.mode}") String mode,
                             UserRepository userRepository,
                             PasswordEncoder passwordEncoder) {
        this.profileCount = profileCount;
        this.mode = mode.toLowerCase();
        this.userRepository = userRepository;
        this.faker = new Faker();
        this.random = new Random();
        this.password = passwordEncoder.encode("test");
    }

    @PostConstruct
    public void init() {
        log.info("[test data generator] activated. Mode: {}", mode);
        if ("generator".equals(mode)) {
            log.info("[test data generator] generating data cache");
            for (int i = 0; i < 1000; i++) {
                firstNames.add(i, faker.name().firstName());
                lastNames.add(i, faker.name().lastName());
                cities.add(i, faker.address().city());
                jobs.add(i, faker.job().title());
                beers.add(i, faker.beer().name());
            }
            log.info("[test data generator] generating data cache complete");
            this.generate();
        } else {
            this.clean();
        }
    }

    private List<User> generateUsersChunk(int chunkSize) {
        return LongStream.range(0, chunkSize).mapToObj(i -> {
            User user = createUser();
            //log.debug("User generated: {}", user);
            return user;
        }).collect(Collectors.toList());
    }

    private void generate() {
        log.info("[test data generator] generating test profiles: {}", profileCount);
        int chunkCount = (int) (profileCount / chunkSize);
        CountDownLatch latch = new CountDownLatch(chunkCount);
        for (int i = 0; i < profileCount; i+=chunkSize) {
            long time = System.currentTimeMillis();
            List<User> users = generateUsersChunk(chunkSize);
            log.info("[test data generator] {} users generated ({}). Time = {}ms", chunkSize, i, System.currentTimeMillis() - time);
            executorService.submit(() -> {
                long insertTime = System.currentTimeMillis();
                userRepository.batchInsertUsers(users);
                log.info("[test data generator] {} users inserted. Time = {}ms", chunkSize, System.currentTimeMillis() - insertTime);
                latch.countDown();
                log.debug("Count down: {}/{}", latch.getCount(), chunkCount);
            });
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            log.error("User latch awaiting error", e);
        }
        log.info("[test data generator] user generating complete");
        log.info("[test data generator] generating friendships...");
        this.generateFriendships();
        log.info("[test data generator] generating friendships complete");
        log.info("[test data generator] generating complete");
    }

    private void generateFriendships() {
        Long minUserId = userRepository.getMinUserId();
        Long maxUserId = userRepository.getMaxUserId();
        AtomicInteger counter = new AtomicInteger(Math.toIntExact(maxUserId - minUserId - 1));

        LongStream.range(minUserId, maxUserId).parallel().forEach(i -> {
            List<Friendship> friendships = new ArrayList<>();
            int friendsCount = 100;
            for (int j = 0; j < friendsCount; j++) {
                Friendship friendship = new Friendship();
                friendship.setUserOneId(i);
                friendship.setUserTwoId(random.nextInt((int) (maxUserId - minUserId + 1)) + minUserId);
                friendships.add(friendship);
                if (friendships.size() % 100 == 0) {
                    try {
                        userRepository.batchInsertFriendships(friendships);
                        friendships.clear();
                    } catch (Exception ignored) {}
                }
            }
            if (friendships.size() > 0) {
                try {
                    userRepository.batchInsertFriendships(friendships);
                } catch (Exception ignored) {}
            }
            log.info("[test data generator] generating friendships for user id = {} complete. User count remaining = {}",
                    i, counter.decrementAndGet());
        });

    }

    private User createUser() {
        User user = new User();
        user.setFirstName(generateFirstName());
        user.setLastName(generateLastName());
        user.setUsername(generateUsername());
        user.setPassword(generatePassword());
        user.setHobbies(generateHobbies());
        user.setCity(generateCity());
        user.setGender(generateGender());
        user.setAge(generateAge());
        return user;
    }

    private Short generateAge() {
        return (short)(random.nextInt(91) + 10);
    }

    private Gender generateGender() {
        return (random.nextInt(100) % 2 == 0) ? Gender.MALE : Gender.FEMALE;
    }

    private String generateCity() {
        return cities.get(random.nextInt(999));
    }

    private String generateFirstName() {
        return firstNames.get(random.nextInt(999));
    }

    private String generateLastName() {
        return lastNames.get(random.nextInt(999));
    }

    private String generateUsername() {
        return UUID.randomUUID().toString() + random.nextInt(100);
    }

    private List<String> generateHobbies() {
        return Arrays.asList(jobs.get(random.nextInt(999)), beers.get(random.nextInt(999)));
    }

    private String generatePassword() {
        return password;
    }

    private void clean() {
        log.info("[test data generator] all test data will be removed");
        userRepository.cleanAll();
        log.info("[test data generator] cleaning complete");
    }
}
