package com.otus.lemon.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(exclude = "password")
public class User {
    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Short age;
    private Gender gender;
    private List<String> hobbies;
    private String city;

    @JsonIgnore
    public String getPassword() {
        return this.password;
    }

    @JsonSetter
    public void setPassword(String password) {
        this.password = password;
    }
}