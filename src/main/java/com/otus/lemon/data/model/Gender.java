package com.otus.lemon.data.model;

public enum Gender {
    MALE,
    FEMALE
}
