package com.otus.lemon.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SearchRequest {
    private String city;
    private String firstName;
    private String lastName;

    @JsonIgnore
    public boolean isEmpty() {
        return city == null && firstName == null && lastName == null;
    }
}
