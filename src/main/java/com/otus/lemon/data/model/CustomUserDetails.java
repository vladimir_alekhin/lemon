package com.otus.lemon.data.model;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomUserDetails extends User {
    private static final long serialVersionUID = 7665636208892524663L;

    @Getter
    private final com.otus.lemon.data.model.User user;

    public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities,
                             com.otus.lemon.data.model.User user) {
        super(username, password, authorities);
        this.user = user;
    }

    public CustomUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
                             boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
                             com.otus.lemon.data.model.User user) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.user = user;
    }
}
