package com.otus.lemon.data.model;

import lombok.Data;

@Data
public class Friendship {
    private Long userOneId;
    private Long userTwoId;
}
