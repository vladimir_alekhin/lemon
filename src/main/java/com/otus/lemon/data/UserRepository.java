package com.otus.lemon.data;

import com.otus.lemon.data.model.Friendship;
import com.otus.lemon.data.model.SearchRequest;
import com.otus.lemon.data.model.User;

import java.util.List;

public interface UserRepository {
    User findById(Long id);
    User findByUsername(String username);
    List<User> findFriendsById(Long id);
    void createFriendship(Friendship friendship);
    void removeFriendship(Friendship friendship);
    User createUser(User user);
    List<User> findAll(SearchRequest request);
    void cleanAll();
    Long getMinUserId();
    Long getMaxUserId();
    void batchInsertUsers(List<User> users);
    void batchInsertFriendships(List<Friendship> friendships);
}
