package com.otus.lemon.data.mapper;

import com.otus.lemon.data.model.Gender;
import com.otus.lemon.data.model.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int i) throws SQLException {
        User user = new User();

        user.setId(rs.getLong("id"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setAge(rs.getShort("age"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setGender(Gender.valueOf(rs.getString("gender")));
        user.setCity(rs.getString("city"));
        user.setHobbies(restoreHobbies(rs.getString("hobbies")));

        return user;
    }

    private List<String> restoreHobbies(String hobbies) {
        if (StringUtils.isEmpty(hobbies))
            return Collections.emptyList();

        return Arrays.asList(hobbies.split(","));
    }
}
