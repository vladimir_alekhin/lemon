package com.otus.lemon.data.mapper;

import com.otus.lemon.data.model.Friendship;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FriendshipMapper implements RowMapper<Friendship> {

    @Override
    public Friendship mapRow(ResultSet rs, int i) throws SQLException {
        Friendship friendship = new Friendship();

        friendship.setUserOneId(rs.getLong("user_one_id"));
        friendship.setUserTwoId(rs.getLong("user_two_id"));

        return friendship;
    }
}
