package com.otus.lemon.data;

import com.otus.lemon.data.mapper.FriendshipMapper;
import com.otus.lemon.data.mapper.UserMapper;
import com.otus.lemon.data.model.Friendship;
import com.otus.lemon.data.model.SearchRequest;
import com.otus.lemon.data.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class JdbcUserRepository implements UserRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public User findById(Long id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", id);
        User user = jdbcTemplate.queryForObject("select * from user where id=:id", namedParameters, new UserMapper());
        log.debug("Requested user={} by id={}", user, id);
        return user;
    }

    @Override
    public User findByUsername(String username) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("username", username);
        User user = jdbcTemplate.queryForObject("select * from user where username=:username", namedParameters, new UserMapper());
        log.debug("Requested user={} by username={}", user, username);
        return user;
    }

    @Override
    public List<User> findFriendsById(Long id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", id);
        List<Friendship> friendships = jdbcTemplate.query(
                "select * from friendship where user_one_id=:id or user_two_id=:id", namedParameters, new FriendshipMapper());

        if (CollectionUtils.isEmpty(friendships))
            return Collections.emptyList();

        SqlParameterSource params = new MapSqlParameterSource().addValue(
                "ids",
                friendships.stream()
                        .map(f -> Objects.equals(f.getUserOneId(), id) ? f.getUserTwoId() : f.getUserOneId())
                        .distinct()
                        .collect(Collectors.toList()));
        List<User> friends = jdbcTemplate.query("select * from user where id in (:ids)", params, new UserMapper());
        log.debug("Found {} friends by user.id={}", friends.size(), id);
        return friends;
    }

    @Override
    public void createFriendship(Friendship friendship) {
        Long userOneId = friendship.getUserOneId() > friendship.getUserTwoId() ? friendship.getUserTwoId() : friendship.getUserOneId();
        Long userTwoId = friendship.getUserOneId() > friendship.getUserTwoId() ? friendship.getUserOneId() : friendship.getUserTwoId();

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("userOne", userOneId)
                .addValue("userTwo", userTwoId);

        jdbcTemplate.update("insert into friendship (user_one_id, user_two_id) values (:userOne, :userTwo)",
                namedParameters);

        log.debug("Friendship created: {}", friendship);
    }

    @Override
    public void removeFriendship(Friendship friendship) {
        Long userOneId = friendship.getUserOneId() > friendship.getUserTwoId() ? friendship.getUserTwoId() : friendship.getUserOneId();
        Long userTwoId = friendship.getUserOneId() > friendship.getUserTwoId() ? friendship.getUserOneId() : friendship.getUserTwoId();

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("userOne", userOneId)
                .addValue("userTwo", userTwoId);

        jdbcTemplate.update("delete from friendship where user_one_id=:userOne and user_two_id=:userTwo",
                namedParameters);

        log.debug("Friendship removed: {}", friendship);
    }

    @Override
    public User createUser(User user) {
        String hobbies = joinHobbies(user.getHobbies());

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("firstName", user.getFirstName())
                .addValue("lastName", user.getLastName())
                .addValue("gender", user.getGender().name())
                .addValue("age", user.getAge())
                .addValue("city", user.getCity())
                .addValue("hobbies", hobbies);

        jdbcTemplate.update("insert into user (username, password, first_name, last_name, age, gender, hobbies, city)\n" +
                        "values (:username, :password, :firstName, :lastName, :age, :gender, :hobbies, :city);",
                namedParameters);

        log.debug("User created: {}", user);

        return user;
    }

    private String joinHobbies(List<String> hobbies) {
        if (CollectionUtils.isEmpty(hobbies))
            return "";

        return String.join(",", hobbies);
    }

    @Override
    public List<User> findAll(SearchRequest request) {

        log.debug("Searching for users. Request: {}", request);

        StringBuilder sb = new StringBuilder("select * from user");

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        if (!request.isEmpty()) {
            sb.append(" where true ");

            if (!StringUtils.isEmpty(request.getCity())) {
                sb.append("and city = :city");
                namedParameters.addValue("city", request.getCity());
            }

            if (!StringUtils.isEmpty(request.getFirstName())) {
                sb.append("and first_name = :firstName");
                namedParameters.addValue("firstName", request.getFirstName());
            }

            if (!StringUtils.isEmpty(request.getLastName())) {
                sb.append("and last_name = :lastName");
                namedParameters.addValue("lastName", request.getLastName());
            }
        }

        String sql = sb.toString();
        log.debug("User search: {}", sql);

        List<User> users = jdbcTemplate.query(sql, namedParameters, new UserMapper());
        log.debug("Searching for users. Result size={}", users.size());
        return users;
    }

    @Override
    public void cleanAll() {
        jdbcTemplate.update("delete from friendship;", new MapSqlParameterSource());
        log.info("All friendship data removed");
        jdbcTemplate.update("delete from user;", new MapSqlParameterSource());
        log.info("All user data removed");
    }

    @Override
    public Long getMinUserId() {
        return jdbcTemplate.queryForObject("select min(id) from user", new MapSqlParameterSource(), Long.class);
    }

    @Override
    public Long getMaxUserId() {
        return jdbcTemplate.queryForObject("select max(id) from user", new MapSqlParameterSource(), Long.class);
    }

    @Override
    public void batchInsertUsers(List<User> users) {
        SqlParameterSource[] sources = new SqlParameterSource[users.size()];
        for (int i = 0; i < users.size(); i++) {
            sources[i] = new MapSqlParameterSource()
                    .addValue("username", users.get(i).getUsername())
                    .addValue("password", users.get(i).getPassword())
                    .addValue("firstName", users.get(i).getFirstName())
                    .addValue("lastName", users.get(i).getLastName())
                    .addValue("gender", users.get(i).getGender().name())
                    .addValue("age", users.get(i).getAge())
                    .addValue("city", users.get(i).getCity())
                    .addValue("hobbies", joinHobbies(users.get(i).getHobbies()));
        }

        this.jdbcTemplate.batchUpdate("insert into user (username, password, first_name, last_name, age, gender, hobbies, city)\n" +
                "values (:username, :password, :firstName, :lastName, :age, :gender, :hobbies, :city);", sources);

        log.debug("Inserted {} users", users.size());
    }

    @Override
    public void batchInsertFriendships(List<Friendship> friendships) {
        SqlParameterSource[] sources = new SqlParameterSource[friendships.size()];
        for (int i = 0; i < friendships.size(); i++) {
            Long userOneId = friendships.get(i).getUserOneId() > friendships.get(i).getUserTwoId() ?
                    friendships.get(i).getUserTwoId() : friendships.get(i).getUserOneId();
            Long userTwoId = friendships.get(i).getUserOneId() > friendships.get(i).getUserTwoId() ?
                    friendships.get(i).getUserOneId() : friendships.get(i).getUserTwoId();

            sources[i] = new MapSqlParameterSource()
                    .addValue("userOne", userOneId)
                    .addValue("userTwo", userTwoId);
        }

        this.jdbcTemplate.batchUpdate("insert into friendship (user_one_id, user_two_id) values (:userOne, :userTwo)", sources);

        log.debug("Inserted {} friendships", friendships.size());
    }
}
