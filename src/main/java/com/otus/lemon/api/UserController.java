package com.otus.lemon.api;

import com.otus.lemon.data.UserRepository;
import com.otus.lemon.data.model.Friendship;
import com.otus.lemon.data.model.SearchRequest;
import com.otus.lemon.data.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class UserController {

    private final UserRepository userRepository;

    @GetMapping("/user")
    public ResponseEntity<?> findUser(@RequestParam(value = "id", required = false) Long id,
                                      @RequestParam(value = "username", required = false) String username) {
        if (id == null && username == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id or username must be provided");
        }

        if (id != null && username != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("only id or only username must be provided");
        }

        if (id != null) {
            User user = userRepository.findById(id);
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found by id=" + id);
            }
            return ResponseEntity.ok(user);
        } else {
            try {
                User user = userRepository.findByUsername(username);
                if (user == null) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found by username=" + username);
                }
                return ResponseEntity.ok(user);
            } catch (EmptyResultDataAccessException e) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found by username=" + username);
            }
        }
    }

    @GetMapping("/user/{id}/friends")
    public ResponseEntity<?> findFriendsByUser(@PathVariable Long id) {
        return ResponseEntity.ok(userRepository.findFriendsById(id));
    }

    @PostMapping("/friendship/create")
    public ResponseEntity<?> follow(@RequestBody Friendship friendship) {
        try {
            userRepository.createFriendship(friendship);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            log.error("Friendship create error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Friendship create error" + e.getMessage());
        }
    }

    @PutMapping("/friendship/remove")
    public ResponseEntity<?> unfollow(@RequestBody Friendship friendship) {
        try {
            userRepository.removeFriendship(friendship);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            log.error("Friendship create error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Friendship create error" + e.getMessage());
        }
    }

    @PostMapping("/user/search")
    public ResponseEntity<?> search(@RequestBody SearchRequest request) {
        return ResponseEntity.ok(userRepository.findAll(request));
    }
}
