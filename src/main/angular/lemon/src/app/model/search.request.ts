export class SearchRequest {
  constructor(
    public city?:string,
    public firstName?:string,
    public lastName?:string
  ) {
  }
}
