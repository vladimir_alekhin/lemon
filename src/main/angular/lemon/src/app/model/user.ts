export class User {

  constructor(
    public username:string,
    public firstName:string,
    public lastName:string,
    public age:number,
    public gender:string,
    public hobbies:string[],
    public city:string,
    public id?:number,
    public password?:string
  ) {
  }
}
