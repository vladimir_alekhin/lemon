export class Friendship {

  constructor(
    public userOneId:number,
    public userTwoId:number
  ) {
  }
}
