import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./component/login/login.component";
import {ProfileComponent} from "./component/profile/profile.component";
import {AuthGuardService} from "./service/auth-guard.service";
import {NotFoundComponent} from "./component/not-found/not-found.component";
import {SearchComponent} from "./component/search/search.component";
import {SignOutComponent} from "./component/sign-out/sign-out.component";
import {SignUpComponent} from "./component/sign-up/sign-up.component";


const routes: Routes = [
  { path:  '', redirectTo:  'profile', pathMatch:  'full' },
  { path:  'lemon', redirectTo:  'profile', pathMatch:  'full' },
  { path:  'login', component: LoginComponent },
  { path:  'sign-up', component: SignUpComponent },
  { path:  'profile/:name', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path:  'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path:  'search', component: SearchComponent, canActivate: [AuthGuardService] },
  { path:  'sign-out', component: SignOutComponent, canActivate: [AuthGuardService] },
  { path:  'not-found', component: NotFoundComponent },
  { path:  '**', component: NotFoundComponent }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
