import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { ProfileComponent } from './component/profile/profile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from "./service/auth.service";
import {HTTP_INTERCEPTORS, HttpClientModule, HttpRequest} from "@angular/common/http";
import {AuthInterceptor} from "./service/auth.interceptor";
import {AuthGuardService} from "./service/auth-guard.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {JwtModule} from "@auth0/angular-jwt";
import {UserService} from "./service/user.service";
import { NotFoundComponent } from './component/not-found/not-found.component';
import { NavigationComponent } from './component/navigation/navigation.component';
import { SearchComponent } from './component/search/search.component';
import { SignOutComponent } from './component/sign-out/sign-out.component';
import { SignUpComponent } from './component/sign-up/sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    NotFoundComponent,
    NavigationComponent,
    SearchComponent,
    SignOutComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    JwtModule.forRoot({config: {tokenGetter: getToken()}})
  ],
  providers: [
    AuthService,
    UserService,
    AuthGuardService,
    { provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getToken() {
  return function (p1: HttpRequest<any>) {
    return localStorage.getItem("tkn");
  };
}
