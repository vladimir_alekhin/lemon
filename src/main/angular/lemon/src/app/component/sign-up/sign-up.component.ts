import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {fallIn, moveIn, moveInLeft} from "../../../../animation";
import {AuthService} from "../../service/auth.service";
import {User} from "../../model/user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class SignUpComponent implements OnInit {

  form: FormGroup;
  genders: Gender[] = [new Gender("MALE", "Мужской"), new Gender("FEMALE", "Женский")];

  constructor(private fb: FormBuilder,
              private as: AuthService,
              private router:Router) {

  }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      age: ['', Validators.required],
      city: ['', Validators.required],
      hobby: ['', Validators.required],
      password: ['', Validators.required],
      passwordTwo: ['', Validators.required]
    }, {
      validators: SignUpComponent.checkPasswords
    });
  }

  signUp() {
    this.as.signUp(
      new User(
        this.form.controls['username'].value,
        this.form.controls['firstName'].value,
        this.form.controls['lastName'].value,
        this.form.controls['age'].value,
        this.form.controls['gender'].value,
        this.form.controls['hobby'].value.split(","),
        this.form.controls['city'].value,
        null,
        this.form.controls['password'].value
      )
    ).subscribe(
      (data) => {
        localStorage.setItem("tkn", data.token);
        this.router.navigateByUrl("/profile");
      },
      (err) => {
        console.error("Sign-up unsuccessful", err);
      });
  }

  private static checkPasswords(group: FormGroup) {
    let pass = group.get('password').value;
    let confirmPass = group.get('passwordTwo').value;

    return pass === confirmPass ? null : {notSame: true}
  }
}

export class Gender {
  constructor(
    public gender: string,
    public name: string
  ) {
  }
}
