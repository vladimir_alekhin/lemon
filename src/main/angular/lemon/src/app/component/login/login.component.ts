import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../service/auth.service";
import {Cred} from "../../model/cred";
import {Router} from "@angular/router";
import {fallIn, moveIn, moveInLeft} from "../../../../animation";
import {JwtHelperService} from "@auth0/angular-jwt";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class LoginComponent implements OnInit {

  form:FormGroup;

  constructor(private fb:FormBuilder,
              private as:AuthService,
              private router: Router,
              private jwtHelper: JwtHelperService) {
    this.form = this.fb.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
  }

  ngOnInit(): void {
  }

  login() {
    this.as.login(new Cred(
      this.form.controls['username'].value,
      this.form.controls['password'].value)).subscribe(
        (data) => {
          localStorage.setItem("tkn", data.token);
          console.log(this.jwtHelper.decodeToken(data.token));
          this.router.navigateByUrl("/profile");
        },
      (err) => {
          console.error(err);
      })
  }
}
