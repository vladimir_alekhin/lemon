import {Component, OnInit} from '@angular/core';
import {fallIn, moveIn, moveInLeft} from "../../../../animation";
import {AuthService} from "../../service/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../service/user.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../../model/user";
import {Friendship} from "../../model/friendship";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class ProfileComponent implements OnInit {

  loggedInUser: User;
  user: User;
  friends: User[];
  imageIndex: number = this.getRandomInt(0, 2);

  private images: string[] = ["assets/1.jpg", "assets/2.jpeg", "assets/3.jpeg"];

  constructor(private as: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private us: UserService,
              private jwtHelper: JwtHelperService) {
  }

  ngOnInit(): void {
    if (!this.as.isAuthenticated()) {
      this.router.navigateByUrl("/login");
    }

    const token = localStorage.getItem("tkn");
    this.loggedInUser = this.jwtHelper.decodeToken(token)['user'];
    console.log("loggedInUser: " + JSON.stringify(this.loggedInUser));

    let username = String(this.activatedRoute.snapshot.paramMap.get('name'));
    console.log("username from route: " + username);

    if (username === undefined ||
      username == null ||
      username == "null" ||
      username === this.loggedInUser.username) {

      console.log("currentUser == loggedInUser");
      this.user = this.loggedInUser;
      this.us.findUserFriends(this.user.id).subscribe(
        (friends) => {
          this.friends = friends;
        },
        (error) => {
          console.error(error);
        })
    } else {
      console.log("Loading current profile");
      this.us.findUserByUsername(username).subscribe(
        (data) => {
          this.user = data;
          this.us.findUserFriends(this.user.id).subscribe(
            (friends) => {
              this.friends = friends;
            },
            (error) => {
              console.error(error);
            })
        },
        (err) => {
          console.error(err);

          if (err.status == 404) {
            this.router.navigateByUrl("/not-found");
          }
        })
    }
  }

  public randomImage() {
    return this.images[this.imageIndex];
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  showFollowBtn() {
    if (!this.friends || !this.loggedInUser)
      return false;

    if (this.loggedInUser.username == this.user.username)
      return false;

    return this.friends.find(f => f.username === this.loggedInUser.username) === undefined;
  }

  follow() {
    this.us.follow(new Friendship(this.loggedInUser.id, this.user.id)).subscribe(
      (ok) => {
        this.friends.push(this.loggedInUser);
      },
      (err) => {
        console.error("Friendship create error", err)
      });
  }

  unfollow() {
    this.us.unfollow(new Friendship(this.loggedInUser.id, this.user.id)).subscribe(
      (ok) => {
        let current:User = this.friends.find(f => f.id == this.loggedInUser.id);
        if (current == undefined)
          return;

        let index = this.friends.indexOf(current);
        this.friends.splice(index, 1);
      },
      (err) => {
        console.error("Friendship remove error", err)
      });
  }
}
