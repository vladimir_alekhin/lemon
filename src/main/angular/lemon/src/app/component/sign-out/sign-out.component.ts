import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {fallIn, moveIn, moveInLeft} from "../../../../animation";

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.scss'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class SignOutComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  signOut() {
    localStorage.removeItem("tkn");
    this.router.navigateByUrl("/login");
  }
}
