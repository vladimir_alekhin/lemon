import { Component, OnInit } from '@angular/core';
import {fallIn, moveIn, moveInLeft} from "../../../../animation";
import {UserService} from "../../service/user.service";
import {User} from "../../model/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SearchRequest} from "../../model/search.request";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class SearchComponent implements OnInit {

  private images: string[] = ["assets/1.jpg", "assets/2.jpeg", "assets/3.jpeg"];
  imageIndex: number = this.getRandomInt(0, 2);
  users:User[] = [];
  searchForm:FormGroup;

  constructor(private us:UserService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      city: ['']
    });
    this.search();
  }

  public randomImage() {
    return this.images[this.imageIndex];
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  search() {
    const sr:SearchRequest = new SearchRequest(
      this.searchForm.controls['city'].value,
      this.searchForm.controls['firstName'].value,
      this.searchForm.controls['lastName'].value
    );

    this.us.search(sr).subscribe(
      (data) => {
        this.users = data;
      },
      (err) => {
        console.error(err);
      });
  }
}
