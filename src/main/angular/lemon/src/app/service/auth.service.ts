import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Cred} from "../model/cred";
import {Token} from "../model/token";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../model/user";

@Injectable()
export class AuthService {

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService) { }

  public login(cred: Cred) {
    return this.http.post<Token>('/api/login', cred);
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('tkn');

    if (token === undefined || token == null) {
      return false;
    }

    return !this.jwtHelper.isTokenExpired(token);
  }

  public signUp(user:User) {
    return this.http.post<Token>("/api/sign-up", user);
  }
}
