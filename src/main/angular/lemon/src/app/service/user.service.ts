import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User} from "../model/user";
import {Friendship} from "../model/friendship";
import {SearchRequest} from "../model/search.request";

@Injectable()
export class UserService {

  constructor(private http:HttpClient) {
  }

  public findUserByUsername(username:string) {
    console.log("Getting User by username: " + username);
    return this.http.get<User>("/api/user?username=" + username);
  }

  public findUserFriends(id:number) {
    return this.http.get<User[]>("/api/user/" + id + "/friends");
  }

  public follow(friendship:Friendship) {
    return this.http.post("/api/friendship/create", friendship);
  }

  public unfollow(friendship:Friendship) {
    return this.http.put("/api/friendship/remove", friendship);
  }

  public search(sr:SearchRequest) {
    return this.http.post<User[]>("api/user/search", sr);
  }
}
