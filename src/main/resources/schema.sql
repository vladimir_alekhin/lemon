DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id int NOT NULL UNIQUE AUTO_INCREMENT,
  username VARCHAR(100) NOT NULL UNIQUE ,
  password VARCHAR(200) NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  age int,
  gender VARCHAR(6) NOT NULL,
  hobbies VARCHAR(500),
  city VARCHAR(100),
  PRIMARY KEY (id)
)
CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

DROP TABLE IF EXISTS friendship;
CREATE TABLE friendship (
    user_one_id int NOT NULL,
    user_two_id int NOT NULL,
    PRIMARY KEY (user_one_id, user_two_id)
)
CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

INSERT INTO user (id, username, password, first_name, last_name, age, gender, hobbies, city)
values (1, 'test', '$2a$10$0c1pez76zdX/GIOIgrLjceveVJtfJYnmhnFRkQY7xtSyrowgSpjUG', 'Тестов', 'Тест', 12, 'MALE', 'есть,спать', 'Урюпинск');

INSERT INTO user (id, username, password, first_name, last_name, age, gender, hobbies, city)
values (2, 'losyash', '$2a$10$0c1pez76zdX/GIOIgrLjceveVJtfJYnmhnFRkQY7xtSyrowgSpjUG', 'Лосяш', 'Лосяш', 12, 'MALE', 'настольные игры,бабочки', 'Санкт-Петербург');
INSERT INTO user (id, username, password, first_name, last_name, age, gender, hobbies, city)
values (3, 'ejick', '$2a$10$0c1pez76zdX/GIOIgrLjceveVJtfJYnmhnFRkQY7xtSyrowgSpjUG', 'Ёжик', 'Ёжик', 12, 'MALE', 'ботаника', 'Санкт-Петербург');

-- INSERT INTO friendship (user_one_id, user_two_id)
-- values (1, 2);
INSERT INTO friendship (user_one_id, user_two_id)
values (1, 3);
INSERT INTO friendship (user_one_id, user_two_id)
values (2, 3);
